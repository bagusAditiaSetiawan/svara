import express from 'express';
import 'express-async-errors';
import { userRouter } from './modules/auth/routes/user';
import { json } from 'body-parser';
import { NotFoundError } from './errors/not-found-error';
import { errorHandler } from './middleware/errror-handler';
import mongoose from 'mongoose';
import fs from 'fs';
import { City } from './modules/location_indonesia/models/cities.model';
import { CityCollection } from './modules/location_indonesia/utills/city.interface';
import { locationIndonesiaRouter } from './modules/location_indonesia/routes/location_indonesia.router';

const app = express();

const createCity = async () => {
    const city = await City.count();
    if(!city) {
        const cityCollection = JSON.parse(fs.readFileSync('city.json', 'utf8')) as CityCollection[];
        const cities = cityCollection.map((value) => ({
            province_id: value.FIELD2,
            name: value.FIELD3
        }))
        await City.insertMany(cities);
    }
}

const connectDB = async () => {
    try {
        await mongoose.connect(process.env.MONGODB_DABASE_URL || '');
        await createCity();
        console.log('DB Connected')
    } catch(err) {
        console.log(err);
    }
}


app.use(json());


app.use('/api/auth', userRouter);
app.use('/api/indonesia', locationIndonesiaRouter);

app.all("*", async (req, res) => {
    throw new NotFoundError();
})    
app.use(errorHandler);

export {app, connectDB};