import { Router} from "express";
import { get } from "../controllers/location_indonesia.controller";

const router = Router();

router.get('/city', get);

export {
    router as locationIndonesiaRouter
}