
export interface CreateCity {
    province_id: number,
    name: string;
}

export interface CityFilter {
    limit: number,
    page: number,
}

export interface CityCollection {
    FIELD1: number,
    FIELD2: number,
    FIELD3: string,
}