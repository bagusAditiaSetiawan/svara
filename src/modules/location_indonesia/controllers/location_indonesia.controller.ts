import { Request, Response } from "express";
import { CityFilter } from "../utills/city.interface";
import { City } from "../models/cities.model";


export const get = async (req: Request<{},{},{}, CityFilter>, res: Response) => {
    const { page = 1, limit= 10 } = req.query;
    const total = await City.count();
    const data = await City.find({}, null, { skip: (page - 1) * limit, limit })
    .exec();
    return res.json({
        total,
        data,
    });
}