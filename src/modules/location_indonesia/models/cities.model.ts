import mongoose, { Model } from 'mongoose';
const { Schema } = mongoose;

export interface CityInterface {
    province_id: number,
    name: string;
}

const citySchema = new Schema<CityInterface, Model<CityInterface>>({
    province_id: Number,
    name: String,
}, {    
    toJSON: {
        transform(doc, ret) {
            ret.id = ret._id;
            delete ret.province_id;
            delete ret._id;
            delete ret.__v;
        }
    }
});

const City = mongoose.model('ms_city', citySchema);

export {
    City
}