import { Model } from "mongoose"
import { User, UserInterface } from "../models/sys_user.model"
import { Password } from "./password"
export class UserService {
    static async isExist(email: string){
        return await User.findOne({
            email
        })
    }

    static async create({email, password, profile}: UserInterface) {
        const hashedPassword = await Password.toHash(password);
        const user = new User<UserInterface>({
            email,
            password: hashedPassword,
            profile,
        });
        await user.save();
        return user;
    }
}
