import { Profile, ProfileInterface } from "../models/profile. model";
export class ProfileService {
    static async create({name, address, hobbies, city}: ProfileInterface) {
        const profile = new Profile({
            name,
            address,
            hobbies,
            city,
        })
        return await profile.save();
    }
}
