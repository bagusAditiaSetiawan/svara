import { body } from "express-validator";

export class AuthRequest {    
    static signin() {
        return [
            body('email').notEmpty().withMessage('Email is required').isEmail().withMessage('Email must be valid'),
            body('password').notEmpty().withMessage('Password is required').isLength({
                min: 8,
            }).withMessage('Password atleast 8 characters').isString().withMessage('Password must be string')
        ]
    }

    static changePassword() {
        return [
            body('password').notEmpty().withMessage('Password is required').isLength({
                min: 8,
            }).withMessage('Password atleast 8 characters').isString().withMessage('Password must be string'),
            body('confirm_password').notEmpty().withMessage('Password is required').isLength({
                min: 8,
            }).withMessage('Password atleast 8 characters').isString().withMessage('Password Confirm must be string'),
        ]
    }

    static signup() {
        return [
            body('name').notEmpty().withMessage('Name is required').isString().withMessage('Name must be string'),
            body('address').notEmpty().withMessage('Address is required').isString().withMessage('Address must be string'),
            body('cityId').notEmpty().withMessage('CityId is required').isString().withMessage('CityId must be string'),
            body('confirm_password').notEmpty().withMessage('Password is required').isLength({
                min: 8,
            }).withMessage('Password atleast 8 characters').isString().withMessage('Password Confirm must be string'),
            body('hobbies').notEmpty().withMessage('Hobbies is required').isArray().withMessage('Hobbies must be array'),
            body('hobbies.*').isString().withMessage('Hobbies must be array string'),
            ...this.signin(),
        ]
    }

}