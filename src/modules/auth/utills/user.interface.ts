export interface UserPayload {
    email: string,
}

export interface UserFilterPagination {
    page: number,
    limit: number
}

export type UserDataPagination = {
    total: number
}


export interface RegisterRequest {
    email: string,
    password: string,
    name: string,
    address: string,
    cityId: string,
    hobbies: string[],
    last_login?: Date,
    confirm_password: string,
}