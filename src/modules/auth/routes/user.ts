import { Router} from "express";
import { changePassword, destory, detail, get, signin, signup} from "../controllers/user.controller";
import { ValidationRequest } from "../../../middleware/validation-request";
import { AuthRequest } from "../request/auth";
import { currentUser } from "../../../middleware/current-user";
import { requireAuth } from "../../../middleware/require-auth";

const router = Router();

router.post('/login', AuthRequest.signin(), ValidationRequest, signin);
router.post('/register', AuthRequest.signup(), ValidationRequest, signup);
router.put('/change-password', currentUser, requireAuth, AuthRequest.changePassword(), ValidationRequest, changePassword);
router.get('/account', get)
router.get('/account/:id', detail)
router.delete('/account/:id', destory)


export {
    router as userRouter
}