import { Request, Response } from "express";
import { UserService } from "../services/user.service";
import { RegisterRequest, UserFilterPagination, UserPayload } from "../utills/user.interface";
import { JWT } from "../services/jwt";
import { Password } from "../services/password";
import { User } from "../models/sys_user.model";
import { RequestValidationError } from "../../../errors/request-validation-error";
import { NotAutorizeError } from "../../../errors/not-authorize-error";
import { NotFoundError } from "../../../errors/not-found-error";
import { City } from "../../location_indonesia/models/cities.model";
import { ProfileService } from "../services/profile.service";


export const get = async (req: Request<{},{},{}, UserFilterPagination>, res: Response) => {
    const { page = 1, limit= 10 } = req.query;
    const total = await User.count();
    const data = await User.find({}, null, { skip: (page - 1) * limit, limit })
        .exec();
    return res.json({
        total,
        data,
    });
}

export const detail = async (req: Request, res: Response) => {
    const {id} = req.params;
    const user = await User.findById(id).populate({
        path: 'profile',
        populate: {
            path: 'city'
        }
    }).exec().catch(() => null);
    if(!user) throw new NotFoundError();
    return res.json(user);
}


export const signup = async (req: Request, res: Response) => {
    const { name, email, password, confirm_password, address, cityId, hobbies } = req.body as RegisterRequest;
    if(password !== confirm_password) throw new RequestValidationError([{
        value: undefined,
        msg: 'Password is not match',
        param: 'confirmPassword',
        location: 'body'
      }
    ]);

    const isCityExist = await City.findOne({
        _id: cityId
    }).exec().catch(() => null);
    

    if(!isCityExist) throw new RequestValidationError([{
        value: undefined,
        msg: 'City is not exist',
        param: 'cityId',
        location: 'body'
      }
    ]);

    const isEmailExist = await UserService.isExist(email);
    if(isEmailExist) throw new RequestValidationError([{
        value: undefined,
        msg: 'Email is exist',
        param: 'email',
        location: 'body'
      }
    ]);

    const profile = await ProfileService.create({
        city: isCityExist._id,
        name, address, hobbies,
    });
    
    await UserService.create({email, password, profile: profile._id});

    res.status(201).json({
        message: "Success register"
    });
}

export const signin = async (req: Request, res: Response) => {
    const { email, password} = req.body;
    const user = await UserService.isExist(email);
    if(!user) throw new NotAutorizeError('Email is not founded');

    const isMatch = await Password.compare(user.password, password);
    if(!isMatch) throw new NotAutorizeError('Password is wrong');

    user.last_login = new Date();
    await user.save();
 
    const userPayload: UserPayload = {
        email: user.email,
    };

    const token = await JWT.generateToken(userPayload);
    res.json({
        message: "Success login",
        token,
    });
}

export const changePassword = async (req: Request, res: Response) => {
    const { password, confirm_password } = req.body as RegisterRequest;
    if(password !== confirm_password) throw new RequestValidationError([{
        value: undefined,
        msg: 'Password is not match',
        param: 'confirmPassword',
        location: 'body'
      }
    ]);
    const user = await User.findOne({
        email: req.user?.email,
    }).exec().catch(() => null);
    if(!req.user || !user) {
        throw new NotFoundError();
    }
    user.password = await Password.toHash(password);
    user.save();
    res.json({
        message: 'change password is success'
    })
}


export const destory = async (req: Request, res: Response) => {
    const {id} = req.params;
    const user = await User.findById(id).exec().catch(() => null);
    if(!user) throw new NotFoundError();
    await user.deleteOne();   
    return res.status(201).json({message: "User is success deleted"});
}