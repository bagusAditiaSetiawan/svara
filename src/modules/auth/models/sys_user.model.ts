import mongoose, { Model, PopulatedDoc, Document, ObjectId, Types,  } from 'mongoose';
import { Schema } from 'express-validator';
const { Schema } = mongoose;

export interface UserInterface {
    email: string,
    password: string,
    last_login?: Date,
    confirmPassword?: string,
    profile?: PopulatedDoc<Document<ObjectId> & UserInterface>,
}

const SysUserSchema = new Schema<UserInterface, Model<UserInterface>>({
    email: String,
    password: String,
    last_login: {
        type: Date,
        default: Date.now(),
    },
    profile: {
        type: Types.ObjectId,
        ref: 'profile'
    },
}, {
    timestamps: { 
        createdAt: 'created_at', 
        updatedAt: 'updated_at'
    },    
    toJSON: {
        transform(doc, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.password;
            delete ret.__v;
        }
    }
});


const User = mongoose.model('sys_user', SysUserSchema);

export {
    User
}