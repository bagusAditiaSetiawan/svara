import mongoose, { Document, Model, ObjectId, PopulatedDoc, Types } from 'mongoose';
import { CityInterface } from '../../location_indonesia/models/cities.model';
const { Schema } = mongoose;

export interface ProfileInterface {
    city?: PopulatedDoc<Document<ObjectId> & CityInterface>,
    name: string;
    address: string;
    hobbies: string[];
}

const profileSchema = new Schema<ProfileInterface, Model<ProfileInterface>>({
    city: {
        type: Types.ObjectId,
        ref: 'ms_city'
    },
    address: String,
    name: String,
    hobbies: [String],
}, {
    toJSON: {
        transform(doc, ret) {
            delete ret._id;
            delete ret.__v;
        }
    }
});

const Profile = mongoose.model('profile', profileSchema);

export {
    Profile, profileSchema
}